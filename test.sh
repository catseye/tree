#!/bin/sh -x

# SPDX-FileCopyrightText: In 2014, Chris Pressey, the original author of this work, placed it into the public domain.
# SPDX-License-Identifier: Unlicense
# For more information, please refer to <https://unlicense.org/>

./script/tree script || exit 1
